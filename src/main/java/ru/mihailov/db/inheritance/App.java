package ru.mihailov.db.inheritance;

import org.hibernate.Session;
import ru.mihailov.db.inheritance.model.ApplicationSettings;
import ru.mihailov.db.inheritance.model.DeveloperSettings;
import ru.mihailov.db.inheritance.session.HibernateUtil;

public class App {

    public static void main(String[] args) throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        DeveloperSettings developerSettings = new DeveloperSettings();
        developerSettings.setHost("192.168.0.1");
        developerSettings.setPort(80);
        developerSettings.setEmail("1@mail.ru");
        developerSettings.setFullName("Philipp Morise");
        session.save(developerSettings);
        session.close();

        session = HibernateUtil.getSessionFactory().openSession();
        ApplicationSettings applicationSettings = new ApplicationSettings();
        applicationSettings.setHost("localhost");
        applicationSettings.setPort(443);
        applicationSettings.setMemory(1024);
        applicationSettings.setOperationSystem("Ubuntu");
        session.save(applicationSettings);
        session.close();

    }

}
