package ru.mihailov.db.inheritance.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

import static ru.mihailov.db.inheritance.config.DatabaseConfig.DB_TABLE_PREFIX;

@Data
@Entity
@Table(name = DB_TABLE_PREFIX + "application_settings")
public class ApplicationSettings extends Settings {

    private String operationSystem;

    private Integer memory;

}
